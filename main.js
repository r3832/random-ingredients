let INGREDIENTS = require('./ingredients.json');
let _ = require('lodash');

let PARAMS = {
    "omni": {
        "proteinNumber": 1,
        "carbNumber": 1,
        "vegetableNumber": 3,
        "specialIngredientNumber": 2
    },
    "hardmode": {
        "proteinNumber": 2,
        "carbNumber": 1,
        "vegetableNumber": 4,
        "specialIngredientNumber": 4
    }
}

console.log(getRandomIngredients(PARAMS["omni"]));

function getRandomIngredients(params) { 
    let randomIngredients = [];

    randomIngredients = randomIngredients.concat(getRandomIngredientsLabel(INGREDIENTS.protein, params.proteinNumber));
    randomIngredients = randomIngredients.concat(getRandomIngredientsLabel(INGREDIENTS.carb, params.carbNumber));
    randomIngredients = randomIngredients.concat(getRandomIngredientsLabel(INGREDIENTS.vegetable, params.vegetableNumber));

    let specialIngredients = {...INGREDIENTS.dairy,
        ...INGREDIENTS.fruit, ...INGREDIENTS.herbs, ...INGREDIENTS.nuts, 
        ...INGREDIENTS.other, ...INGREDIENTS.spices};

    randomIngredients = randomIngredients.concat(getRandomIngredientsLabel(specialIngredients, params.specialIngredientNumber));

    return randomIngredients;
}

function getRandomIngredientsLabel(ingredientsAsJSON, numberOfSample) {
    let randomIngredients = _.sampleSize(Object.values(ingredientsAsJSON), numberOfSample);
    
    let ingredientsLabels = [];
    _.each(randomIngredients, function(randomIngredient) {
        ingredientsLabels.push(randomIngredient.label);
    })
    
    return ingredientsLabels;
}